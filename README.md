##High Level

This algorithm is developed from the ACP control times

The website to calculate all of the times is https://rusa.org/pages/acp-brevet-control-times-calculator and with info here https://rusa.org/pages/rulesForRiders

This calculator, as described in lecture, is a good model to base my algorithm on https://rusa.org/octime_acp.html

I'm using AJAX and Flask for this application and the calculator

#New additions for project 5!

Added a Display and Submit button, here are all of the updates that you should know in regards to that.

As discussed in office hours, use the km boxes for testing input, as was done for project 4. I added a neat feature where whenver you insert kilometers, the program will ask you to round the miles to a whole integer, so please just use the km boxes. Here are the test cases/functionalities of the buttons:

Display (DB empty): Pressing Display with nothing in the db, redirects to error.html Pressing with entries inserted but not submitted, redirects to error.html (Ex/ You entered 20.2 in a km box but didn't press submit)

Display (DB has entries): Pressing Display will redirect to display.html and display all entries.

Submit: Pressing submit with nothing in the input fields goes to error.html, pressing it with data in input fields enters the values into the databse and clears all entries.

NOTE: You can control C the connection, run ./run.sh and clear all entries in the database.

#New additions for project 6!

Here are the four project parts in detail, enter in these ports to properly navigate

http://localhost:5002/ should lead you to project 5 in full

http://localhost:5000/ is pretty much the consumer program that implements everything below

    Added a RESTful service to expose what is stored in MongoDB.
        "http://localhost:5001/listAll" should return all open and close times in the database
        "http://localhost:5001/listOpenOnly" should return open times only
        "http://localhost:5001/listCloseOnly" should return close times only

    Added two different representations: one in csv and one in json.
        "http://localhost:5001/listAll/csv" should return all open and close times in CSV format
        "http://localhost:5001/listOpenOnly/csv" should return open times only in CSV format

        "http://localhost:5001/listCloseOnly/csv" should return close times only in CSV format

        "http://localhost:5001/listAll/json" should return all open and close times in JSON format
        "http://localhost:5001/listOpenOnly/json" should return open times only in JSON format
        "http://localhost:5001/listCloseOnly/json" should return close times only in JSON format

    Added a query parameter to get top "k" open and close times. For examples, see below.
        "http://localhost:5001/listOpenOnly/csv?top=3" should return top 3 open times only (in ascending order) in CSV format
        "http://localhost:5001/listOpenOnly/json?top=5" should return top 5 open times only (in ascending order) in JSON format
        "http://localhost:5001/listCloseOnly/csv?top=6" should return top 5 close times only (in ascending order) in CSV format
        "http://localhost:5001/listCloseOnly/json?top=4" should return top 4 close times only (in ascending order) in JSON format

#New additions for project 7!

POST /api/register

This new resource will register a new user, and a 201 code will be returned if that is successful. The response will be a displayed JSON object that has that new username, and a location header will have the URI. A failure prompts a 400 to be called. All the hashing that's needed (hashing the password, discarding the original password) is included with a database that has a id, username, and password for fields (the id is unique).

GET /api/token

This resource will give you a token using HTTP Basic Authentication like password.py. A JSON object is returned on a successful venture with a field token that is set to the auth token that the user gets and a duration field set to the length of validation for the token. A failure prompts a 401.

GET/Project 6 Resource

A protected resource is returned (project 6). This is done using token-based authentication (like testToken.py). A JSON object with the appropriate data for the authenticated user will be returned, and a 401 is a failure.

There's also a user interface portion, where I created a quick UI using Flask-WTF and Flask-Login. The interface allows you to register to be a new user, to sign in, and to log out. You also have the ability to have the app remember you, and you have CSRF protection throughout. The UI grants the ability for basic authentication and token generation as well.

##Running

Use the scripts provided in the /DockerRestAPI directory: ./run.sh will build everything and ./remove.sh will be cleanup.

##Clearing ambiguity

I'm not letting times above the local max brevet count, they just default to that max brevet, I'm not doing much rounding either as the calculator is doing, the times are a bit more arbitrary but shouldn't mess up testing.

##User vs Developer

As a user, if you're planning on making another route, or you have messed up your current route, it's best to refresh the page and start over. There's nothing really for developers to know, the front end is whole, just be sure to refresh as well.

Name: Will Christensen Email: wwc@uoregon.edu
