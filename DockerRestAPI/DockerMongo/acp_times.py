"""
Open and close time calculations
for ACP-sanctioned brevets
following rules described at https://rusa.org/octime_alg.html
and https://rusa.org/pages/rulesForRiders
"""
import arrow

checkpoints = [200, 400, 600, 1000, 1300]
maximums = [34, 32, 30, 28, 26]
minimums = [15, 15, 15, 11.428, 13.333]

def open_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
           in kilometers, which must be one of 200, 300, 400, 600,
           or 1000 (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control open time.
       This will be in the same time zone as the brevet start time.
    """
    arrow_var = arrow.get(brevet_start_time)
    currHour = 0
    i = 0
    
    if control_dist_km > brevet_dist_km:
       control_dist_km = brevet_dist_km
    remains = control_dist_km
   
    while i < 5:
       if i != 0:
          inbetween = min(remains, (checkpoints[i]-checkpoints[i-1]))
       else:
          inbetween = min(remains, checkpoints[i])

       remains -= inbetween
       currHour += round(inbetween/maximums[i],2)
       i += 1

    if control_dist_km < 0:
       currHour = 0
    return arrow_var.shift(hours = currHour).isoformat()



def close_time(control_dist_km, brevet_dist_km, brevet_start_time):
    """
    Args:
       control_dist_km:  number, the control distance in kilometers
       brevet_dist_km: number, the nominal distance of the brevet
          in kilometers, which must be one of 200, 300, 400, 600, or 1000
          (the only official ACP brevet distances)
       brevet_start_time:  An ISO 8601 format date-time string indicating
           the official start time of the brevet
    Returns:
       An ISO 8601 format date string indicating the control close time.
       This will be in the same time zone as the brevet start time.
    """
    arrow_var = arrow.get(brevet_start_time)
    currHour = 0
    i = 0
    
    if control_dist_km > brevet_dist_km:
       control_dist_km = brevet_dist_km
    remains = control_dist_km
   
    while i < 5:
       if i != 0:
          inbetween = min(remains, (checkpoints[i]-checkpoints[i-1]))
       else:
          inbetween = min(remains, checkpoints[i])

       remains -= inbetween
       currHour += round(inbetween/minimums[i],2)
       i += 1
       
    if currHour == 0:
       currHour = 1
    return arrow_var.shift(hours = currHour).isoformat()
