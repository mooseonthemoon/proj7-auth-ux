# Laptop Service

from flask import Flask, request, Response, render_template
from flask_restful import Resource, Api, reqparse
from pymongo import MongoClient

import flask
import json
import csv

import logging
import pymongo
from pymongo import MongoClient

from flask_login import LoginManager, login_user, logout_user, login_required
from flask_wtf import FlaskForm, CSRFProtect
from wtforms import Form, StringField, PasswordField, BooleanField, validators
from wtforms.validators import InputRequired, Length

from base64 import b64decode
from bson.objectid import ObjectId 
from password import hash_password, verify_password
from testToken import generate_auth_token, verify_auth_token, token_required

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient("db", 27017)
db = client.tododb
app.config['SECRET_KEY'] = "Howdypartner117078"

collection = db.control #
login_manager = LoginManager()
login_manager.init_app(app)
csrf = CSRFProtect(app) #
csrf_token = "117078howdypartner" #

# Use as reference
# class Laptop(Resource):
#     def get(self):
#         return {
#             'Laptops': ['Mac OS', 'Dell', 
#             'Windozzee',
# 	    'Yet another laptop!',
# 	    'Yet yet another laptop!'
#             ]
#         }

class User():
	def __init__(self, user_id):
		self.user_id = user_id

	def is_authenticated(self):
		return True

	def is_active(self):
		return True

	def is_anonymous(self):
		return False 

	def get_id(self):
		return self.user_id

class SignupForm(FlaskForm):
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=20)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=100)])

class LoginForm(FlaskForm):
	username = StringField('username', validators=[InputRequired(), Length(min=4, max=20)])
	password = PasswordField('password', validators=[InputRequired(), Length(min=4, max=100)])
	remember = BooleanField('please just remember me')

class registerUser(Resource):
	def post(self):

		# Parsing needed arguments
		parse = reqparse.RequestParser()
		parse.add_argument('username', required=True, help="the username cannot be empty")
		parse.add_argument('password', required=True, help="the password cannot be empty")
		args.parse.parse_args()

		get_user = args['username']
		get_pass = args['password']

		# Logic to determine user request
		if collection.users.find_one({"username":get_user}) != None:
			return {"client error":"username exists already"}, 418

		if get_user == None or get_pass == None:
			return {"client error":"incorrect request"}, 400
		else:
			passhash = hash_password(get_pass)
			hashpassID = collection.users.insert_one({"username":get_user, "password":passhash})
			ID = str(hashpassID.inserted_id)
			get_pass = None
			return flask.jsonify({"success":"created","username":get_user,"location":ID}), 201

class getToken(Resource):
	def get(self):
		authCred = request.headers.get("Authorization")

		if authCred != None:
			cred = authCred.split(' ')
			decoding = b64decode(cred[1]).decode()

			user = decoding.split(':')
			get_pass = user[1]
			get_user = user[0]

			doc = collection.users.find_one({"username":get_user})
			if not verify_password(get_pass, doc['password']):
				return {"unauthorized":"wrong password"}, 401
			if doc == None:
				return {"unauthorized":"the user cannot be found"}, 401

			token = generate_auth_token(expiration=2000)
			return {"token":token.decode(), "duration":2000}, 200
		else:
			return {"unauthorized":"No header found for Authorization"}, 401

@login_manager.user_loader
def load_user(user_id):
	unique_id = str(user_id)
	user = collection.users.find_one(ObjectId(unique_id))

	if user == None:
		return None
	return User(user['_id'])

@app.route('/')
def index():
	return render_template('index.html')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
	form = SignupForm()

	if form.validate_on_submit():
		get_user = form.username.data
		get_pass = form.password.data

		if collection.users.find_one({"username":get_user}) != None:
			flask.flash("This particular username is in use")
			return render_template('signup.html', form=form)
		elif get_user == None or get_pass == None:
			flask.flash("Nothing has been entered to the fields")
			return render_template('signup.html', form=form)

		passhash = hash_password(get_pass)

		hashpassID = collection.users.insert_one({"username":get_user, "password":passhash})
		ID = str(hashpassID.inserted_id)
		get_pass = None

		return flask.jsonify({"success":"created","username":get_user,"location":ID}), 200
	else:
		return render_template('signup.html', form=form)

@app.route('/login', methods=['GET', 'POST'])
def login():
	form = LoginForm()

	get_user = form.username.data
	get_pass = form.password.data
	remember = form.remember.data

	if form.validate_on_submit() and request.method == 'POST':
		user = collection.users.find_one({"username":get_user})

		if not verify_password(get_pass, user['password']):
			flask.flash("this is the wrong password, unauthorized move")
			return render_template('login.html', form=form)
		elif user == None:
			flask.flash("user cannot be found, this is unauthorized")
			return render_template('login.html', form=form)

		# User ID and token logic discussed in class
		ID = str(user['_id'])
		user_object = User(ID)
		login_user(user_object, remember=remember)

		token = generate_auth_token(expiration=2000)
		return flask.jsonify({"token":token.decode(), "duration":2000}), 200
	else:
		return render_template('login.html', form=form)

@app.route('/logout')
@login_required
def logout():
	logout_user()
	return "You have been logged out"

class _listAll(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list],
			'_close_time': [i['_close'] for i in result_list]
		}

class _listOpenOnly(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list]
		}

class _listCloseOnly(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_close_time': [i['_close'] for i in result_list]
		}

class _listAllJson(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list],
			'_close_time': [i['_close'] for i in result_list]
		}

class _listOpenOnlyJson(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list]
		}

class _listCloseOnlyJson(Resource):
	@login_required
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_close_time': [i['_close'] for i in result_list]
		}

# From my understanding csv should be a couple of lines changed, nothing more.

class _listAllCsv(Resource):
	@login_required
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_open'] + ', ' + item['_close'] + ', '

		return retStr

class _listOpenOnlyCsv(Resource):
	@login_required
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_open'] + ', ' 

		return retStr


class _listCloseOnlyCsv(Resource):
	@login_required
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_close'] + ', '

		return retStr

# Create routes
# Another way, without decorators
#api.add_resource(Laptop, '/')

api.add_resource(_listAll, '/listAll')
api.add_resource(_listOpenOnly, '/listOpenOnly')
api.add_resource(_listCloseOnly, '/listCloseOnly')
api.add_resource(_listAllJson, '/listAll/json')
api.add_resource(_listOpenOnlyJson, '/listOpenOnly/json')
api.add_resource(_listCloseOnlyJson, '/listCloseOnly/json')
api.add_resource(_listAllCsv, '/listAll/csv')
api.add_resource(_listOpenOnlyCsv, '/listOpenOnly/csv')
api.add_resource(_listCloseOnlyCsv, '/listCloseOnly/csv')
api.add_resource(registerUser, '/api/register')
api.add_resource(getToken, '/api/token')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
